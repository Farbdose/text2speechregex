#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import codecs
import os
from shutil import copy
from ColoredDifflib.colored_difflib import color_diff
import regex as re
import time
from colorama import init

init(wrap=False)
from colorama import Fore, Back, Style

if len(sys.argv) < 2 or sys.argv[1].startswith("-"):
    print("No input file specified!")
    print("Defaulting to 'Text2Speach Regex TestSuite.txt'")
    input_file = "Text2Speach Regex TestSuite.txt"
elif not os.path.isfile(sys.argv[1]):
    print("File not found!")
    exit(1)
else:
    input_file = sys.argv[1]

input_file, file_extension = os.path.splitext(input_file)
dictionary = "english.lex"
output = input_file + ".result.txt"
log = input_file + ".log.rtf"
copy(input_file+file_extension, output)
norep = "-norep" in sys.argv
byfind = "-byfind" in sys.argv
verbose = "-v" in sys.argv
n = int(sys.argv[sys.argv.index("-n") + 1]) if "-n" in sys.argv else 1

stats = []

print_all = "-all" in sys.argv

def rtf_encode_char(unichar):
    code = ord(unichar)
    if code < 128:
        return str(unichar)
    return '\\u' + str(code if code <= 32767 else code-65536) + '?'


def rtf_encode(unistr):
    return ''.join(rtf_encode_char(c) for c in unistr)

def colorama2rtf(stri):
    stri = stri.replace("\\", "\\\\")
    stri = stri.replace(r"{",r"\{")
    stri = stri.replace(r"}", r"\}")
    stri = stri.replace("\n", "\\par \n")
    stri = stri.replace(Fore.CYAN, "{\\cf6 ")
    stri = stri.replace(Style.BRIGHT + Fore.GREEN,"{\\cf4 ")
    stri = stri.replace(Fore.GREEN, "{\\cf5 ")
    stri = stri.replace(Fore.RED, "{\\cf3 ")
    stri = stri.replace(Back.GREEN, "{\\highlight5 ")
    stri = stri.replace(Back.RED, "{\\highlight3 ")
    stri = stri.replace(Style.BRIGHT, "")
    stri = stri.replace(Style.RESET_ALL, "}")
    #stri = stri.replace(r"[", r"\[")
    #stri = stri.replace(r"]", r"\]")
    #stri = stri.replace(r"(", r"\(")
    #stri = stri.replace(r")", r"\)")
    stri = rtf_encode(stri)
    return stri


with open(log, "w+") as log:
    log.seek(0)
    log.truncate()
    log.write(r'{\rtf1\formshade\landscape\paperh31811\paperw31811\margtsxn208\margbsxn208\margl0\margr0\margt0\margb0'
              r'\viewbksp1{\*\background{\shp{\*\shpinst{\sp{\sn shapeType}{\sv 1}}{\sp{\sn fillColor}{\sv 2361904}}}}}'
              r'{\fonttbl {\f0\froman\fprq2\fcharset0 Ubuntu Mono;}}\fs20\viewscale140'
              r'{\colortbl;\red48\green10\blue36;\red255\green255\blue255;\red197\green2\blue20;\red138\green225\blue48;\red78\green154\blue6;\red0\green200\blue200;}\f0')
    log.write("Input: %s. \\par\nOutput: %s" % (input_file, output))
    with codecs.open(dictionary, "r", "utf16") as f:
        reg_exen = f.readlines()
        with codecs.open(output, "r+", "utf16") as g:
            content = g.read().replace(r'\\\\\"', '"') + "\n"
            last_comment_line = ""
            i_flag = False
            for line in reg_exen:
                line = line.strip()
                #            print(line)
                if line == "":
                    pass
                elif line[0:2] == "# ":
                    last_comment_line = line
                elif line[0:1] != "#":
                    pattern = re.sub(r'" "(.*[^\\])?"$', '', line)[1:].replace('\\"',
                                                                               '"')  # .replace("{","\\{").replace("}","\\}")
                    substitute = re.sub(r'\\(\d)(?=\d)', r'\\g<\1>',
                                        re.sub(r'\\"', '"', re.sub(r'^".*?[^\\]" "', '', line)[:-1]).replace('\\"',
                                                                                                             '"'))

                    # extract flags
                    if re.search(r"\(\?[^\)]*\-i[^\)]*\)", pattern):
                        i_flag = False
                    elif re.search(r"\(\?[^\)]*i[^\)]*\)", pattern):
                        i_flag = True

                    # fix inline flags
                    modify = False
                    if re.search(r"\(\?[^\)]*\-[im][^\)]*\)", pattern):
                        modify = True
                        while re.search(r"\(\?[^\)]*\-[im][^\)]*\)", pattern):
                            pattern = re.sub(r"(\(\?[^\)]*)\-[im]([^\)]*\))", r"\1\2", pattern)
                        pattern = re.sub(r"\(\?\)", "", pattern)

                    if i_flag and not re.match(r"\(\?[^\)]*i[^\)]*\)", pattern):
                        pattern = "(?i)" + pattern

                    t = time.time()
                    for i in range(n):
                        pattern_count = len(re.findall(pattern, content))
                    counting = (time.time() - t) / n

                    sys.stdout.flush()
                    content_bak = content
                    if pattern_count > 0:
                        t = time.time()
                        if not norep:
                            for i in range(n):
                                content = re.sub(pattern, substitute, "" + content_bak)
                        replacing = (time.time() - t) / n
                    else:
                        replacing = counting

                    if (print_all or pattern_count > 0) and last_comment_line != "":
                        stri = "\n" + Fore.CYAN + last_comment_line + Style.RESET_ALL
                        log.write(colorama2rtf(stri+"\n"))
                        print(stri)
                        last_comment_line = ""

                    if print_all or pattern_count > 0 or modify:
                        stri = "%s%6d x count: %5.5f, sub: %5.5f  '%-70s %s '%s' %s" % (
                            Style.BRIGHT + Fore.GREEN if pattern_count > 0 else "", pattern_count, counting, replacing,
                            pattern + "'", "~~>" if modify else "==>", substitute, Style.RESET_ALL if pattern_count > 0 else "")
                        log.write("{\\b %s\\par\n}" % colorama2rtf(stri))
                        print(stri)

                    if 0 < pattern_count < 1000 and verbose:
                        diff_str = color_diff(content_bak, content).replace("\n", "\n        ")
                        if len(diff_str) > 2000:
                            diff_str = diff_str[0:2000] + "...\n"
                        stri = colorama2rtf("        " + diff_str)
                        log.write(stri)
                        #log.write(""+str(len(re.findall("\\{\\\\c",stri)))+"?="+str(len(re.findall("[^\\\\]\\}",stri))))
                        log.write("}" if len(re.findall("\\{\\\\[ch]",stri)) !=len(re.findall("[^\\\\]\\}",stri)) else "")
                        log.write(colorama2rtf("\n"))
                        print("        %s" % (diff_str + Style.RESET_ALL))

                    stats.append((pattern_count, counting, replacing, pattern, modify, substitute))

            g.seek(0)
            g.write(content)
            g.truncate()

            if verbose and len(content) < 100000:
                log.write("\\par\n\\par\n%s\\par\n" % colorama2rtf(content))
                print("\n\n%s" % content)

    with codecs.open("stats.csv", "w+", "utf16") as f:
        f.seek(0)
        stats = sorted(stats, key=lambda a: a[1 if norep or byfind else 2])

        log.write(colorama2rtf("\n\n" + "=" * 10 + "Stats" + "=" * 10 + "\n\n"))
        print("\n\n" + "=" * 10 + "Stats" + "=" * 10 + "\n")

        for i, entry in enumerate(reversed(stats)):
            f.write(u"%d;;%f;;%f;;%s;;%s;;%s\r\n" % entry)
            if i < 10:
                entry = entry[0:4] + ("~~>" if entry[4] else "==>",) + entry[5:]
                stri = "%s%5d x count: %5.5f, sub: %5.5f  '%-70s' %s '%s' %s" % (
                    (Style.BRIGHT + Fore.GREEN,) + entry + (Style.RESET_ALL,))
                log.write("{\\b %s}" % colorama2rtf(stri+"\n"))
                print(stri)

        f.truncate()

    log.write('}\n\x00')
