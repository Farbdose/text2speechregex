# Text2SpeechRegex

A collection of regular expressions for normalizing web content in a way that a [Ivona voice](https://www.ivona.com/us/about-us/voice-portfolio/) can speak it properly.

...and a python script to emulate the internal regex engine of said voices to simulate the result of applying these regex to a text.