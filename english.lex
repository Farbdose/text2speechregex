#& encoding=UTF-16
#################################################################
#       The IVONA Text-to-Speech exceptions dictionary          #
#                                                               #
#          -----------English language------------              #
#                                                               #
#      Copyright (C) 2003-2011 IVONA Software Sp. z o.o.        #
#                   http://www.ivona.com                        #
#                                                               #
#################################################################
#                                                               #
#                Text normalization by Farbdose                 #
#                                                               #
#          Note:  Text comments should start with "# "          #
#          while disabled regex should start with "#"           #
#                                                               #
#          WARNING: Be careful with negative look behinds       #
#                   they may have unforseen side-effects!!!     #
#                                                               #
#################################################################


" - Gravity Tales" ""

"\\n" "\n"

# oooohhhhuuuu
"(?<=\d)ooooo" "00000"
"(?<=\d)oooo" "0000"
"(?<=\d)ooo" "000"
"(?<=\d)oo" "00"
"(?<=\d)o" "0"

# \r is overrated
"\r\n?" "\n"

# replace line feed
"\f" " "

# feet
"(?<=\d)\’(?=\W|$)" " feet"

# ~ used as - between numbers
"(?<=\d)~(?=\d)" "-"

# ~ for number
"~(?=\d)" "around "

# dash breakline capture
"(?m)^[-–─—一¯]{2,}$" "#M150#"

# ZERO WIDTH NO-BREAK SPACE (is used as escape character by this file)
"﻿" ""

# transforming characters
"\^\.\^" " "
"\^|「|」" " "
"。|·" "."
"[\：\:]" ":"
"(?<=[a-z]):(?! |\n|$)" ": "
"\）|\)" ") "
"\（|\(" " ("
"[《》⟪⟫<>«»”“″‴]+" "\""
"[，|,] *" ","
",(?!\d|\")" ", "
"[′’‘]" "'"
"(…|—)" "..."
"\.\.+" "…"
"[~↑–─—一♪♡]" "-"
"_" " "

# offensive words
"(?i)\bF(?:\*|\.)cker\b" "Fucker"
"(?i)\bf(?:\*|\.)cking\b" "fucking"
"(?i)\bF(?:\*\*|\.\.)ker\b" "Fucker"
"(?i)\bf(?:\*\*|\.\.)king\b" "fucking"
"(?i)\bf\*\*\*\*\*g(?=\b)" "fucking"
"(?i)\bF\*\*\*(?=\b)" "Fuck"
"(?i)\bF(?:\*|\.)ck\b" "Fuck"
"(?i)\bF…(?!= ?F)" "Fuck"
"(?i)sh\*t" "shit"
"(?i)\bD\*mn\b" "Damn"
"(?i)\bD\*mmit\b" "Dammit"
"(?i)\bH\*ll\b" "Hell"
"(?i)(son of a b)\*{2,5}" "\1itch"
"(?i)\bb\*t.?ches\b" "bitches" 
"(?i)\bwtf\b" "What the fuck"
"(?i)\bomg\b" "Oh my god"
"(?i)\bstfu\b(?-i)" "shut the fuck up "

# words wrapped in *
"\*([a-z]{3,})\*" "[\1]"

# break line
"(?m)^ *(([\.\*\-_+=#<>★×※#…0]{1,2})|#PBREAK#)( *\1 *){2,}.{0,4}$" "#M150#"
"(?m)^ *■ *$" "#M150#"
"(?m)^([\.\*\-_+=#<>★×※# ]*)break([\.\*\-+=#<>★×※# ]*)$" "#M150#"
"#PBREAK#" "gibberish"

# stats table break inserter
"\][\t ]?\[" "]!\n["
"(^|\n)[\- ]*?(Act|ACT)(?=. *?[:\-]? *\d+.{0,30}(?=\n|$))" "\1Chapter"
"(?im)^([a-z /]{1,20}?[^\.:])(?:\.?:[\t ]+|\t[\t ]*)([^\t\n:]{1,30}?)\s*\(([a-z /]{1,20})(?::)([^\t\n:]{1,20}?)\)!?\s+(?=[^\n]+:)" "\1: \2.\n\3: \4.\n"
"(?im)^([a-z /]{1,20}?[^\.:])(?:\.?: +)([^\t\n:]{1,15}?(?<=[^\.]))!?\t+([a-z /]{1,20}?[^\.:])(?:\.?: +)([^\t\n:]{1,15}?[^\.:])!?[\t ]+([a-z /]{1,20}?[^\.])(?:\.?: +)([^\t\n:]{0,15}?[^\.:\n])!?$" "\1: \2. \3: \4. \5: \6."
"(?im)^([a-z /]{1,20}?[^\.:])(?:\.?:[\t ]+|\t[\t ]*)([^\t\n:]{1,15}?(?<=[^\.]))!?[\t ]+([a-z ]{1,20}?[^\.:])(?:\.?:[\t ]+|\t[\t ]*)([^\t\n:]{1,15}?[^\.:])!?[\t ]+([a-z ]{1,20}?[^\.])(?:\.?:[\t ]+|\t[\t ]*)([^\t\n:]{0,15}?[^\.:\n])!?$" "\1: \2. \3: \4. \5: \6."
"(?im)^([a-z /]{1,20}?[^\.:])(?:\.?: +)([^\t\n:]{1,15}?(?<=[^\.]))!?\t+([a-z /]{1,20}?[^\.:])(?:: +)([^\t\n:]{0,15}?[^\.:\n])!?$" "\1: \2. \3: \4."
"(?im)^([a-z /]{1,20}?[^\.:])(?:\.?:[\t ]+|\t[\t ]*)([^\t\n:]{1,15}?(?<=[^\.]))!?[\t ]+([a-z ]{1,20}?[^\.:])(?:\.?:[\t ]+|\t[\t ]*)((?:[^\t\n:]|\d\.\d){0,15}?[^\.:\n])!?$" "\1: \2. \3: \4."
"(?im)^([a-z /]{1,20}?[^\.:])(?:\.?:[\t ]+|\t[\t ]*)([^\t\n:]{0,15}?[^\.:\n])!?$" "\1: \2."

# now we don't need tabs anymore
"(?s) *\t+ *" ", "

# * -> " # TODO what IS this?!?
#"(?i)(?:^|(?<=\W))\*(\w+)\*(?=\W|$)" "\"\1!\""

# insert s before brackets if missing
"(?<! )\(" " ("

# insert spaces after commata if its missing and the comma is followed by a letter
",(?=[a-zA-Z])" ", "

# author specific words
"If you find mistakes, pls tell, thx. I don't like mistakes." ""
"Also feel free to ask for more background information on the world. I am somehow running out of interesting points regarding the small comments at the beginning of the chapters." ""
"(?i)\bCeles\b" "<speak><phoneme alphabet='ipa' ph='zɛlɛs'/></speak>"
"(?i)\bDelia\b" "<speak><phoneme alphabet='ipa' ph='ˈdiːlɪə'/></speak>"
"(?i)\bShan shan\b" "shanshaan"
"(?-i)(?<=[a-z])\]'?(?=s)" "'"

# xxxxx that are no breaklines have to be placeholder
"(?i)x{5,}" "#PBREAK#"

# fix : as we need it for HP
"(?i)(?<=[a-z]) : (?=.)" ": "

# fixing didn't and there's is needed for "there's...shit" (modified version must be before stammering)
"(?i)([a-z])'s(?!'|[a-z])" "\1's "
"(?i)([a-z]n)'t(?!'|[a-z])" "\1't "
"(?i)([a-z])(' | ')s(?!'|[a-z])" "\1's "
"(?i)([a-z]n)(' | ')t(?!'|[a-z])" "\1't "

# Stammer normalization: only '-' is used for stammered words, '…' is exclusive for small breaks, also spaces after affected '-' are removed
" […\-]((?<=\b([a-zA-Z]) .)|(?<=\b([a-zA-Z]{2}) .)) ?(?=\2|\3)" "-"
"[…\-]((?<=\b([a-zA-Z]).)|(?<=\b([a-zA-Z]{2}).)) ?(?=\2|\3)" "-"

# as '…' is now break exclusive we can add a space behind it if its missing
"…(?! )" "… "

# Stammered Words
"(?-i)([a-z])\.\1" "\1-\1"
"(?i)\b(S-)(?=\1{0,5}(Shu-|Sure)\b)" "Shu-"
"(?i)\b(Y-)(?=\1{0,5}(U-|You)\b)" "U-"
"(?i)\b(Wh?-)(?=\1{0,5}(Wa-|Why|What)\b)" "Wa-"
"(?i)\bWha+-(?!\w)" "Wah-"
"(?i)\b(Wh?-)(?=\1{0,5}(Wher-|Where)\b)" "Wher-"
"(?i)\b(No?-)+Nothing\b" "<speak><phoneme alphabet='ipa' ph='​​​ˈnʌθ-ˈnʌθɪŋ​​​'/></speak>"
"(?i)\b(H-)+Hello\b" "<speak>Ha<s>Hello</speak>"
"(?i)\b[Tt]-(th|TH)(?=([aeiouAEIOU])[a-z]*\b)" "The-th"
"(?i)\b([Tt]-)(?=\1{0,5}th|TH)" "The-"

# stammering at word end is not supported
"(?-i)(\b[a-zA-Z]{2,20})([a-z])-\2\b" "\1\2"

# stammered word regex up to B...B...Boyfriend and I...Impossible or F...Fiancé and Fl...Fl...Flower 
"(?i)\b(?<![a-z]')(([^aeiou\W0-9]{1,2})-)+(?=[a-z]{2,})(\2[^aeiou\W0-9]{0,3}[aeiou])" "\3-\3"
"(?i)\b(?<![a-z]')(([aeu])-)+(\2[^aeiou\W])" "\3-\3"

# ce-a fix because ca is spoken as see-ay otherwise
"(?i)\bca-ca(?!\b)" "cay-ca"

# fixing I...I don't know?
"(?i)\bI… ?(?=I\b)" "<speak><phoneme alphabet='ipa' ph='​​​aɪ​​​'/><s></speak>"

# more general version accepting all words like ab... abcde (stammered words with vocal in stammered part)
"(?i)\b([aeiou][a-z]{0,2})-(\1-)*(\1[a-z]{2,})" "\1-\3"

# fixing 'I' in stammered words
"(?i)\b(?:((?![aeou])([A-Za-z][a-z]{0,2})?i)-)+(\1)(?!d)(?=\w)" "\2ee-\3"

# fixing Single letter sentece "I." For some reason the dot is ignored if there is only one space after
"(?-i)I\. (?=\w)" "I.  "

# fixing left over Y- 
"(?i)\bY(?:-|…)" "<speak><phoneme alphabet='ipa' ph='​​​j​​ɛ'/></speak>"

# fixing cut off words + ignore words that already contain [aeiou] (+cut off CAPS-" at end of direct speech)
"(?-i)\b([b-df-hj-np-tv-z]{1,3})\-\"" "<speak><phoneme alphabet='ipa' ph='​​​\1'/><break time='0.3'></speak>"
"([a-zA-Z])\-\"" "\1,\""

# remove dots from caps regex (must be after stammering)
"(?-i)(([A-Z]\.){1,}[A-Z])\.(?=\W)" "\1 "
"(?-i)(?<=[A-Z])\.(?=[A-Z](\.|\W))" ""

#"(?-i)(?<![A-Z][- \.])\b((?=[A-Z]+\b)([B-DF-HJ-NP-TV-Z]+|(?!SH|CH)[B-DF-HJ-NP-TV-Z]{2}[A-Z]+))\b(?![- \.][A-Z])" "<speak><say-as interpret-as='spell-out'>\1</say-as></speak>"
"\b(PE)\b" "<speak><say-as interpret-as='spell-out'>\1</say-as></speak>"

# repeat fixing didn't and there's as it must be actually after caps dots remove but is needed for "there's...shit" too
"(?i)([a-z])'s(?!'|[a-z])" "\1's "
"(?i)([a-z]n)'t(?!'|[a-z])" "\1't "
"(?i)([a-z])(' | ')s(?!'|[a-z])" "\1's "
"(?i)([a-z]n)(' | ')t(?!'|[a-z])" "\1't "

# timestamps with AM and PM
"(?-i)\b(\d)(\.|:)(\d\d) ?(AM|PM|am|pm)\b" "\1\3 \4"
"(?-i)\b0?(\d)(\.|:)(\d\d) ?(AM|PM|am|pm)\b" "\1\3 \4"
"(?-i)\b(\d\d)(\.|:)00 ?(AM|PM|am|pm)\b" "\1 hundred \3"
"(?-i)\b(\d\d)(\.|:)(\d\d) ?(AM|PM|am|pm)\b" "\1 hundred \3 \4"
"(?-i)(?<=\d) ?(AM|am)" "A-M"
"(?-i)(?<=\d) ?(PM|pm)" "P-M"

# digital timer ala 01:05:17
"(?<=^|\W)00:00:(\d\d)(?=\W|$)" "\1sec "
"(?<=^|\W)00:(\d\d):00(?=\W|$)" "\1min "
"(?<=^|\W)(\d\d):00:00(?=\W|$)" "\1h "
"(?<=^|\W)00:(\d\d):(\d\d)(?=\W|$)" "\1min and \2sec "
"(?<=^|\W)(\d\d):00:(\d\d)(?=\W|$)" "\1h and \2sec "
"(?<=^|\W)(\d\d):(\d\d):00(?=\W|$)" "\1h and \2min "
"(?<=^|\W)(\d\d):(\d\d):(\d\d)(?=\W|$)" "\1h, \2min and \3sec "
"(?<=^|\W)00:(\d\d)(?=\W|$)" "\1min "
"(?<=^|\W)(\d\d):(\d\d)(?=\W|$)" "\1min and \2sec "

# 4 digit timestamps
"(?-i)\b(?<![£\+\-\*\\])0([1-9])00\b(?!-| ?(AM|PM|am|pm))" "\1 hundred hours "
"(?-i)\b(?<![£\+\-\*\\])(1[0-9]|2[0-4])00\b(?!-| ?(AM|PM|am|pm))" "\1 hundred "
"(?-i)(?<= )(0[0-9])((?:0|1)[0-9]|2[0-4])\b(?!-| ?(AM|PM|am|pm))" "\1:\2 "

# 4m24s
"(?-i)(?<=^|\W)(\d?\d)m(in ?)?( and )?(\d?\d)s(?=\W|$)" "\1min and \4sec "
"(?-i)(?<=^|\W)(?<![Ll]evel )(\d+)s(?=\W|$)" "\1sec "

# fix Date Point
"(?-i)\b(\d{1,2}y)( ?\d{1,2}m)?( ?\d{1,2}w)?( ?\d{1,2}d)?\b" "\1, \2, \3, \4"
"(?-i)\b(\d\d[myw])(?=\d|\W)" "\1, "
"(?-i)\b(\d[myw])(?=\d|\W)" "\1, "
"(?-i)(, {1,2}){2,}" ", "

# insert space after numbers (if they are missing) if the appended word is longer than two character
"(?-i)(\d)([a-zA-Z]{3,})" "\1 \2"

# insert space before numbers if they are prepended by a alphabetical character
"(?i)(\d)(?<=[^#\d][a-z].)" " \1"

# Big numbers 3nd generation REGEX
"(?-i)\b([1-9]\d{0,2}),(\d{3})(,(\d{3})(,(\d{3}))?)?(?!,\d)(\.\d+)?(?!\d|\.\d|,\d|k)" "\1\2\4\6\7"
"(?-i)\b([1-9]\d{0,2})\.(\d{3})(\.(\d{3})(\.(\d{3}))?)?(?!\.\d)(\,\d+)?(?!\d|\.\d|,\d|k)" "\1\2\4\6\7"
"(?-i)\b([1-9]\d{0,2}) (\d{3})( (\d{3})( (\d{3}))?)?(?! \d)(?!\d|\.\d|,\d|k)" "\1\2\4\6"

# replace leftover comma in numbers with decimal dot
"(\d)(?<=[^,].),(\d)(?!\d*,)" "\1.\2"

# all commas with a number afterwards that are left are actually missing spaces
",(?=\d)" ", "

# fix n times
"×(\d)" "× \1"

# Numbers
"(\d+)\-(\d+)\-(\d+)(?:\-(\d+))?(?:\-(\d+))?(?:\-(\d+))?" "\1 \2 \3 \4 \5 \6"
"(?-i)M(?<=\d.)(?!\w)" "million"
"(?<=11|12|13) ?th(\- |\b)" "th "
"(?<=1) ?st(\- |\b)" "st "
"(?<=2) ?nd(\- |\b)" "nd "
"(?<=3) ?rd(\- |\b)" "rd "
"(?<=[4-9]|0) ?ths?(\- |\b)" "th "
"\bn-th\b" "<speak><phoneme alphabet='ipa' ph='ɛn-tð'/></speak>"
"RMB\b" "YEN"
"(?<=\d|st|nd|rd|th) ?\-+ ?(?=[1-9](?:\d|,|\.)*)" " to "
" \.(?=\d)" " 0."
"(?<=[a-z]\.)(?=\d)" " "

"(?-i)(?<=\d)(?!cm|st|nd|rd|th)(?!h|m|s|d|w|y|x)(?=[A-Za-z])" " "
"(?<=\d)x(?!\d)" " times"

# remove + used as escape char (must be before remove leading zeros)
"(\+{2})(\w{2,6})\1" "\2"

# chapter breaks (must be before remove leading zeros)
"(?im)^[\- ]*?(Chapter|Volume|Part). *?(\d+)(?!\d) ?[:\-\.,]? ?(.{2,}?)!?$" "#M250#\n#escape#\1 \2: \3!\n#M150#"
"(?im)^[\- ]*?(Chapter|Volume|Part) +(.{0,50})$" "#M250#\n#escape#\1 \2!\n#M150#"
"(?s-i)#escape#" ""

# remove leading zeros
"(?m)(?<= |^)0+(?=\d)(?!:\d)" ""

# fixing more datepoints and timespans
"(\d)y,? ?1m" "\1y, 1 month"
"(\d)y,? ?(?<!1m)(\d+)m" "\1y, \2 months"
"(\d)y ?(\d+[wd])" "\1y \2"
"\b1w\b" "1 week"
"\b1d\b" "1 day"
"\b(\d+)w\b" "\1 weeks"
"\b(\d+)(?<!3 )d\b" "\1 days"

# rewrite 100+ to over 100
"\b(\d+)\+(?!.{0,3}\d)" "over \1"

# fix 1:1000
"1:(?=\d)" "1 to "

# replace fractions
"\b1/2\b" "½"
"\b1/3\b" "⅓"
"\b1/4\b" "¼"
"\b3/4\b" "¾"
"\b1/5\b" "⅕"
"\b1/6\b" "⅙"

# mark all digits except the first 4 of numbers between 10000 and 99999999999999 
"(?-i)\b(?=[1-9])(\d{3})(?=\d{2,12}(?:[^\d]|[\.,][^\d]|$))(\d*[1-9]\d*)" "\1#:_\2_:#"

# replace all digits except the first 4 of numbers between 100000 and 999999999 with zero
"\d(?=\d*_:#)" "0"

# clean up temporary markers
"#:_|_:#" ""

# replace ending 000s with ks
"\.?000(?=(000)*([^\d]|\n|$))(?![\.,]\d)(?!th)" "k"

# insert dot for big numbers and remove zeros before k (this is all to generate phrases like "22.3 million"
"\b-?([1-9])(?=\d{3}k)(\d+?)0*k" "\1.\2kk"
"\b-?([1-9]\d{1,2})(?=\d{3})(\d+?)0*k" "\1.\2kk"

# prevent "dot zero million"
"(?-i)\b(?=[1-9])(\d{0,2})\.0{1,3}(?=k)" "\1"

# 1/7 to 3 of 4
"\b(\d+k*)/(\d+k*)\b" "\1 of \2"

# replace k with trillion/billion/million/thausend (?![\.,]?\d) is managing measurements
"(?-i)(?<=\d) ?(?!kg|km|kilo)kkkk(?![\.,]?\d|[a-z])" " trillion"
"(?-i)(?<=\d) ?(?!kg|km|kilo)kkk(?![\.,]?\d|[a-z])" " billion"
"(?-i)(?<=\d) ?(?!kg|km|kilo)kk(?![\.,]?\d|[a-z])" " million"
"(?-i)(?<=\d) ?(?!kg|km|kilo)k(?![\.,]?\d|[a-z])" " thousand"

# dash after floating point
"(\d)\.(\d)\-(?!\d)" "\1.\2 "

# bug fix "in 10000" -> "indianer 10000"
"in (?=\d)" "in  "

# misc / abbreviations
"(?-i)\bTL\.?\b" "Translator"
"(?i)(^|\n),{5,50} was brought to you by .{2,200}(\n|$)" ""
"(?i)(Sponsor(ed)?|Translat(or( Check)?|ed)|Proofread(er)?)( by |: ).{2,200}?(\n|#M...#|$)" ""
"(?i)(^|\n).{0,50}translation group.{0,50}(\n|$)" ""
"(?m)^Advertisement$" ""
"(?i)\blv(l?(s?))?\b" "level\2"
"(?i)\bnxt level\b" "next level"
"(?i)(?!\b(II|III)\b)([iI])\1+(?<![XV]I)(?<![XV]II)(?<![XV]III)" "\1"
"(?<![a-z])\?{3,5}" "unknown!"
"(?i)(?<=\d) {0,3}P.M.(?= [a-z])" " PM "
"(?i)(?<=\d) {0,3}A.M.(?= [a-z])" " AM "
"(?i)\b(MP|mp)\b" "M-P"
"(?-i)(HP|hp)\b((?<=\b..)|(?<=\d..))" " H-P"
"(?i)\bMMORPG\b" "M-M-O-R-P-G"
"(?i)\bRPG\b" "R-P-G"
"(?i)\bA\.I\." "A-I"
"(?i)\bA\.?I\.?\b" "A-I"
"(?i)\bAIs\b" "<speak><say-as interpret-as='spell-out'>A</say-as>eyes</speak>"
"(?i)\bPK\b" "P-K"
"(?i)\bPKed\b" "P-Kait"
"(?i) ?/ ?m(in)?\b" " per minute"
"(?i) ?/ ?s(ec)?\b" " per second"
"(?i) ?/ ?h(our)?\b" " per hour"
"(?i) ?/ ?d(ay)?\b" " per day"
"(?-i)\bK[mM]\b" "km"
"(?-i)\bK[gG]\b" "kg"
"(?-i)\bkm(?<=[a-z][a-z][a-z] km)\b" "kilometers"
"(?-i)\bkg(?<=[a-z][a-z][a-z] kg)\b" "kilograms"
"(?-i)\bcm(?<=[a-z][a-z][a-z] cm)\b" "centimeters"
"(?<=\b|\d)Y\b" "y"
"(?i)\bpl[zs]\b" "please"
"(?i)\bthx\b" "thanks"
"(?i)\bY/N\b" "Yes - No"
"(?i)\bExp\b(?!-exp)" "Experience"
"(?i)\bStr\.?\b(?!-s)" "strength"
"(?i)\bStam?\.?\b(?!-s)" "stamina"
"(?i)\bDex\.?\b(?!-d)" "dextority"
"(?i)\bAgi\.?\b(?!-a)" "agility"
"(?i)\bSP\.?\b" "Skill Points"
#"(?-i)((?<=\n|^)|[^\n]{5,})End\.(?!\.)" "\1endurance"
#"(?-i)((?<=^)|(?<=[^\.\!\?] \b))End(?! of)(?=:? +\+?\d|\b[^\n]{4,})" "endurance"
"(?-i)(?<=\b|^)END(?=:? +\+?\d)" "endurance"
"(?i)\bInt\.?\b(?!-i)" "intelligence"
"(?i)\bWis\.?\b(?!-w)" "wisdom"
"(?i)\bMnt\.?\b(?!-m)" "mental fortitude"
"(?i)\bChr\.?\b(?!-c)" "charisma"
"(?i)\bVit\.?\b(?!-v)" "vitality"
"(?i)\bFth\.?\b(?!-f)" "faith"
"(?i)\bCha\.?\b(?!-c)" "chance"
"(?i)\bReg\.?\b(?!-r)" "regen"
"(?i)\bL[uc]k\.?\b" "luck"
"(?i)\bice" "<speak><phoneme alphabet='ipa' ph='ˈaɪs'/></speak>"
"(?i)\bPOV\b" "Point of View"
"(?i)\bN/A\b" "Not available"
"(?i)\bhel\b" "haell"
"(?i)\bI'mma\b" "I'm going to"
"(?-i)([a-zA-Z]{2,20})2([a-zA-Z]{2,20})" "\1 to \2"
"(?i)\bOnee-?chan\b" "big sis"
"(?i)\bmc\b" "main character"
"(?i)\bG(.)\1gle\b" "Google"
"(?-i)\bAV\b" "After Vancoover"
"(?-i)(?<=[a-z])/(?=[a-z])" "-"
"(?-i)\-ist(?<=[a-zA-Z]{3}.{4})\b" "ist"
"(?-i)(?<=\b)(?<!-)GP?(?=\b)" "gold"
"(?-i)\bPP(\b|s\b(?!\.))" "path points"
#"(\d)\-ish" "\1ish"
"(?<=[1-9]) ?vs? ?(?=\d)" " versus "

# Fix mind spoken as minute (insert ZERO WIDTH NO-BREAK SPACE on left to keep following dot alive)
"(?<=\b)(mind)(?=\b)" "﻿\1"

# TODO Creates problems with "array" what was original usage??
#"(?-i)(?<!w|d|s|l)ay\b" "aih"

# fix DPSers
"(?-i)\b([A-Z]{2,4})er(s)?\b" "\1-er\2"

# TODO find out usage of this regex - original regex was: "(\w{3,}) ?\( ?s ?\)" -> "\1s"
"\((?<=\w\w\w\() ?s ?\)" "s"
"\((?<=\w\w\w \() ?s ?\)" "s"

# fix ?!?!
"(\?+!+|!+\?+)" "?"

# Sounds
"(?i)\bTch\b" "<speak><phoneme alphabet='ipa' ph='ˈʧ'/></speak>"
"(?i)\bTs+k?\b" "<speak><phoneme alphabet='ipa' ph='ts|'/></speak>"
"(?i)(?<=\b|r)(hr+)(?=(hr+)+|\b)" "hir "
"(?i)(?<=\b|i)(hi+)(?=(hi+){2,})" "he "
"(?i)\b(hi+){2,}\b" "he he"
"(?i)\ba+r+g+h+\b" "argh"
"\bMs(?!\.)\b" "Ms."
"\bMr(?!\.)\b" "Mr."
"(?i)\b([hrmngpf]{2,10}|chi|pf+t?)\b(?<!HP)(?!\=)(?<!PM)(?<!MP)(?<!PR)((?<!Mr)|(?!\.))" "<speak><phoneme alphabet='ipa' ph='\1'/></speak>"
"(?i)\bcrunch[^\w\n ]?\b" "<speak><phoneme alphabet='ipa' ph='krʌnʧ'/></speak>"
"(?i)\bXXX\b" "X"
"(?i)\bNoo+\b" "no-no-no"
"(?i)\bPs+t\b" "<speak><phoneme alphabet='ipa' ph='pˈst'/></speak>"
"(?i)\bStoo+p\b" "<speak><phoneme alphabet='ipa' ph='st​​​​ɑ​​​p'/></speak>"
"(?i)\bHe+y+\b" "Hey"
"(?i)((Ha+)+h)\b" "\1a"
"(?i)(?<=[ah]|\b)Ha+(?=[ha]*\b)" " Hah "
"(?i)\ba+[ha]+\b" " ah "
"(?i)\bkya\b" "kyaa"
"(?i)\b(p)?s+h+\b" "<speak><phoneme alphabet='ipa' ph='\1ʃ'/></speak>"
"(?-i)([AEIOUaeiou])\1\1+(?<![VX]III)(?<!\bII\b)(?<!\bIII\b)" "\1"
"(?-i)([A-Z])\1\1+(?<![VX]III)(?<!\bII\b)(?<!\bIII\b)" "\1\1"
"(?-i)([a-z])\1\1+" "\1\1"

# inch fix
"(?<=\d)\"" " \""

# begin of list items
"(?im)^ *[x] +(?=\w)" ""

# tab 	fallback
"\t" "#MMED#"

# point of view break
"(?im)^(.{3,20}Point of view.{0,3})$" "#M150#\n\1!\n#MSTR#"
"(?im)^(.{0,3}Point of view.{3,20})$" "#M150#\n\1!\n#MSTR#"

# Author commments and navigation
"(?i)\bNext Chapter|Previous Chapter|Back to main Page|Table of Contents\b" ""
"\(Author.?s? (Comment|Note): [^)]{1,400}\)" ""
"^Author.?s? Comment:?" ""
"^Author.?s? Corner.*" ""
"^Author.?s? Basement.*" ""
"(?-i)^AN\b.*" ""

# + should only be used with numbers, but maybe there is just a space in the way, lets check
"\+ (?=\d)" "+"
"(^|[^\d\+])\+\+?([^\d\+]|$)" "\1 \2"

# add spaces around + again
" ?\+ ?" " + "

# prevent overwrite of ? or ! by breaks
"!{2,}" "!"
"\?{2,}" "?"
"[ \"\,\.']+([?!])(?! |\n|$)" "\1 " 
"^([^a-zA-Z0-9]*)[?!]+" "\1 ..."
"(\?|!)(?=\w)" "\1 "
"\?(?<=\W\?)" " ?"
"!(?<=\W!)" " !"
 
# advanced direct speech fix
"(^|\n)\"(.+)\" ?\[(.{3,16})\]|\((.{3,16})\)($|\n)" "\1\3\4: \"\2#MSTR#" 
 
# reduce spaces
"   +" " "

# Dash, ...
"\-([\.\!\?])" "\1"
"\-((?<=^\- |\W\-)|(?<=\W\- |^-)|(?<=^-))(-| )*(?=\W|$)" " #MMED#"
"\-( ?#M...#)?(?=\W|$)" " #MMED#"
"(?<=#M...#) ?\-(?!\d)" " #MMED#"

 
# Final sentence and paragraph break fix
"(?<=[a-zA-Z]) (?=,)" ""
"(… {0,3})+" " #MSMA# "
"(?<=[\"'])(?<!#M...#)\." " #MSTR# "
"'(?<!#M...#')(?<!'ipa')(,|(?<!s') )" ", "
"([\.\!\?]) +," "\1"
"(?<=\W)\." " #MSTR# " 
"(, ){2,}" ", "
"(?<=\W)," " #MMED# "
"(?-i)'(?=[A-Z])(?<!ph=')" "' "

# add space to lines ending with punctuation mark !IMPORTANT! candidate for 'best fix ever'
"(?m)(\.|!|\?)$" "\1 "

# add space after ! or ? if followed by []
"(!|\?)(\[|\])" "\1 \2"

# replace single ./!/? line with simple break
"(?im)^ *[\.!?] *$" "<speak><break strength='medium'></speak>"

# have to do linebreaks on our own now
"\r?\n\r?\n" " #MSTR# "
"\r?\n" " #MMED# "

# Breaks
" ?(#M...#) +(#M...#) ?" "\1\2"
"(#M...#)*#M250#(#M...#)*" "<speak><break time='0.1'></speak>"
"(#M...#)*#M150#(#M...#)*" "<speak><break time='0.1'></speak>"

"(#M...#)*#MSTR#(#M...#)*" "<speak><break time='0.15'></speak>"
"((#M...#))*#MMED#((#M...#))*" "<speak><break time='0.01'></speak>"
"(#M...#)*#MSMA#(#M...#)*" "<speak><break time='0.075'></speak>"

#"(#M...#)*#MSTR#(#M...#)*" "<speak><break time='0.3'></speak>"
#"((#M...#))*#MMED#((#M...#))*" "<speak><break time='0.2'></speak>"
#"(#M...#)*#MSMA#(#M...#)*" "<speak><break time='0.1'></speak>"

#"(#M...#)*#MSTR#(#M...#)*" "<speak><break time='0.5'></speak>"
#"((#M...#))*#MMED#((#M...#))*" "<speak><break time='0.3'></speak>"
#"(#M...#)*#MSMA#(#M...#)*" "<speak><break time='0.15'></speak>"

# looks like a new sentence without a space after the dot
# TODO write down issue when disabling next time...
"(?-i)\.(?<=[a-z].)(?=[A-Z0-9])" ". "
"(?-i)\.(?=[A-Z][a-z])" ". " 

# fix all leftover dots between letters (prevents "dot")
"(?-i)(?![A-Z]\.[A-Z])(?<=[a-z])\.(?=[a-z])" "-"

# fix "one-ten MP" bug in 110MP TODO seems to be working without?!
#"(?<=\d[^\.,])(?<!\d)(\d{2,})(?= [A-BD-JL-Z])" "<speak>\1</speak>"

# abbreviations
"(?<=^|[[:punct:][:space:]])abbrev\.?(?=[[:punct:][:space:]]|$)" "abbreviated"
"(?<=^|[[:punct:][:space:]])ack\.(?=[[:punct:][:space:]]|$)" "acknowledgement"

# split words
"(?<=^|[[:punct:][:space:]])JPEG(?=[[:punct:][:space:]]|$)" "j-peg"
"(?<=^|[[:punct:][:space:]])readme(?=[[:punct:][:space:]]|$)" "read-me"

# X-SAMPA pronunciation
"(?<=^|[[:punct:][:space:]])WYSIWYG(?=[[:punct:][:space:]]|$)" "<speak><phoneme alphabet='x-sampa' ph='&quot;wI.zi%wIg'/></speak>"
"(?<=^|[[:punct:][:space:]])vis-[aà]-vis(?=[[:punct:][:space:]]|$)" "<speak><phoneme alphabet='x-sampa' ph='%vi:.zA:\"vi:'/></speak>"

